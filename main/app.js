// arr1 = [2, 3, 4,5];
// arr2 = [6, 3, 1,5];

function distance(first, second){
    //throwing exception when one of the parameters aren't arrays
    if(!Array.isArray(first) || !Array.isArray(second)){
        throw new Error("InvalidType");
	} 
	
	//eliminating doubles
	first = [...new Set(first)]
    second = [...new Set(second)]

	let dist = 0
	let length = first.length + second.length;
    first.forEach(element1 => {
		let q = 0
        second.forEach(element2 => {
			// console.log(element2)
            if (element1 === element2) {
				q = 1;
			}
			
        });
		if(q)
			dist++;
	});

    return length - 2*dist;
}

// console.log(distance(arr1, arr2));


module.exports.distance = distance